import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductListService} from '../../shared/services/product-list.service';

import { faClipboardList, faPlus, faTrashAlt, faEdit, faTimes } from '@fortawesome/free-solid-svg-icons';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-custom-liste',
  templateUrl: './custom-liste.page.html',
  styleUrls: ['./custom-liste.page.scss'],
})
export class CustomListePage implements OnInit {

  faFont = {
    faClipboardList,
    faPlus,
    faTrashAlt,
    faEdit,
    faTimes
  };

  id: string;
  listeName: '';
  listeItems: any;

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private listesService: ProductListService,
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.updateDatas();
  }

  updateDatas(){
    const liste = this.listesService.getCustomList(this.id);
    if(liste === undefined){
      this.router.navigate(['liste']);
    }
    this.listeName = liste.nom;
    this.listeItems = liste.produits;
  }

  deleteList(){
    this.listesService.deleteList(Number(this.id));
  }

  changeListName(form: NgForm){

  }
}
