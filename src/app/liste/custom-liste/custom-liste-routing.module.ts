import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomListePage } from './custom-liste.page';

const routes: Routes = [
  {
    path: ':id',
    component: CustomListePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomListePageRoutingModule {}
