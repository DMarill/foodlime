import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomListePageRoutingModule } from './custom-liste-routing.module';

import { CustomListePage } from './custom-liste.page';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ModalModule} from 'ngx-bootstrap/modal';
import {ListePageModule} from '../liste.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CustomListePageRoutingModule,
        FontAwesomeModule,
        ModalModule,
        ListePageModule
    ],
  declarations: [CustomListePage]
})
export class CustomListePageModule {}
