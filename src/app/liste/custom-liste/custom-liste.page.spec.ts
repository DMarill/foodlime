import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CustomListePage } from './custom-liste.page';

describe('CustomListePage', () => {
  let component: CustomListePage;
  let fixture: ComponentFixture<CustomListePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomListePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CustomListePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
