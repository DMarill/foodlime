import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListePage } from './liste.page';

const routes: Routes = [
  {
    path: '',
    component: ListePage
  },
  {
    path: 'custom',
    loadChildren: () => import('./custom-liste/custom-liste.module').then( m => m.CustomListePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListePageRoutingModule {}
