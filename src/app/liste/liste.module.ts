import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListePageRoutingModule } from './liste-routing.module';

import { ListePage } from './liste.page';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ModalModule} from 'ngx-bootstrap/modal';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ListePageRoutingModule,
        FontAwesomeModule,
        ModalModule
    ],
    declarations: [ListePage]
})
export class ListePageModule {}
