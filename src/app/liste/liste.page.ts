import { Component, OnInit } from '@angular/core';
import {ProductsService} from '../shared/services/products.service';

import {faPlusCircle, faTimes} from '@fortawesome/free-solid-svg-icons';
import {NgForm} from '@angular/forms';
import {ProductListService} from '../shared/services/product-list.service';

@Component({
  selector: 'app-liste',
  templateUrl: './liste.page.html',
  styleUrls: ['./liste.page.scss'],
})
export class ListePage implements OnInit {

  faFont = {
    faPlusCircle,
    faTimes,
  };

  constructor(
      public productsService: ProductsService,
      public productListService: ProductListService,
  ) { }

  ngOnInit() {
  }

  createList(form: NgForm): void {
    this.productListService.createList(form.value['nomListe']);
    console.log(form.value['nomListe']);
  }
}
