import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'accueil',
        loadChildren: () => import('../Accueil/accueil.module').then(m => m.AccueilPageModule)
      },
      {
        path: 'liste',
        loadChildren: () => import('../liste/liste.module').then(m => m.ListePageModule)
      },
      {
        path: 'peremption',
        loadChildren: () => import('../peremption/peremption.module').then(m => m.PeremptionPageModule)
      },
      {
        path: 'stock',
        loadChildren: () => import('../stock/stock.module').then(m => m.StockPageModule)
      },
      {
        path: 'parametres',
        loadChildren: () => import('../parametres/parametres.module').then(m => m.ParametresPageModule)
      },
      {
        path: '',
        redirectTo: '/accueil',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/accueil',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
