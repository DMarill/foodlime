import { Component } from '@angular/core';

import { faHome, faListAlt, faCalendarAlt, faBox, faCog } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  faFont = {
    faHome, faListAlt, faCalendarAlt, faBox, faCog,
  };

  constructor() {}

}
