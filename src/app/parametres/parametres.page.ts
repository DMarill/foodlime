import { Component, OnInit } from '@angular/core';
import {DarkModeService} from '../shared/services/dark-mode.service';

import { faSun, faMoon} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-parametres',
  templateUrl: './parametres.page.html',
  styleUrls: ['./parametres.page.scss'],
})
export class ParametresPage implements OnInit {

  faFont = {
    faSun,
    faMoon
  };

  constructor(
      public darkmode: DarkModeService,
  ) { }

  ngOnInit() {
  }

}
