import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AccueilPage } from './accueil.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { AccueilPageRoutingModule } from './accueil-routing.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    AccueilPageRoutingModule,
    FontAwesomeModule
  ],
  declarations: [AccueilPage]
})
export class AccueilPageModule {}
