import { Component, OnInit } from '@angular/core';
import { CalendarOptions } from '@fullcalendar/angular';

@Component({
  selector: 'app-peremption',
  templateUrl: './peremption.page.html',
  styleUrls: ['./peremption.page.scss'],
})
export class PeremptionPage implements OnInit {

  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth'
  };

  constructor() { }

  ngOnInit() {
  }

}
