import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PeremptionPage } from './peremption.page';

const routes: Routes = [
  {
    path: '',
    component: PeremptionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PeremptionPageRoutingModule {}
