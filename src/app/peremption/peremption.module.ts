import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PeremptionPageRoutingModule } from './peremption-routing.module';

import { PeremptionPage } from './peremption.page';
import {FullCalendarModule} from '@fullcalendar/angular';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        PeremptionPageRoutingModule,
        FullCalendarModule
    ],
  declarations: [PeremptionPage]
})
export class PeremptionPageModule {}
