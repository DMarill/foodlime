import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PeremptionPage } from './peremption.page';

describe('PeremptionPage', () => {
  let component: PeremptionPage;
  let fixture: ComponentFixture<PeremptionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeremptionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PeremptionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
