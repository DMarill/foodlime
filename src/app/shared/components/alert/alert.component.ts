import { Component, OnInit } from '@angular/core';
import {AlertService} from '../../services/alert.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
})
export class AlertComponent implements OnInit {

  constructor(
      private router: Router,
      public alertService: AlertService,
  ) {
    router.events.subscribe((val) => {
      this.alertService.reset();
    });
  }

  ngOnInit() {}

}
