import { Injectable } from '@angular/core';
import {ProductsService} from './products.service';
import {AlertController} from '@ionic/angular';
import {AlertService} from './alert.service';
import {RandomStringService} from './random-string.service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProductListService {

  private listes: any;

  constructor(
      private productsService: ProductsService,
      private alertController: AlertController,
      private alertService: AlertService,
      private randomstrService: RandomStringService,
      private router: Router,
  ) {
    console.log(JSON.parse(localStorage.getItem('listes_produits')));
    this.listes = JSON.parse(localStorage.getItem('listes_produits'));
    if(this.listes === null){
      this.listes = {
        courses: [],
        favoris: [],
        custom: [],
      };
    }
  }

  public createList(nom: string){
    this.listes.custom.push({
      nom: nom,
      produits: []
    });
    this.savLists();
    this.alertService.setAlert('Liste créée avec succès');
  }

  public async deleteList(id: number) {
    if (id < 0) {
      console.log('liste invalide');
      return;
    } else {
      const alert = await this.alertController.create({
        header: 'Voulez-vous vraiment supprimer cette liste ?',
        message: 'Les données qui y sont associées ne seront pas récupérables.',
        buttons: [{
            text: 'Annuler',
            cssClass: 'primary',
          },
          {
            text: 'Oui',
            cssClass: 'danger',
            handler: () => {
              this.listes.custom.splice(id,1);
              this.savLists();
              this.router.navigate(['liste']);
          }
        }]
      });
      await alert.present();
    }
  }

  public addToList(liste: number, ean: string){
    if (this.productsService.getProduct(ean) !== null){
      if (liste === -1){
        this.listes.courses.push(ean);
      }else if (liste === -2){
        this.listes.favoris.push(ean);
      }else if (this.listes.custom[liste] !== undefined){
        this.listes.custom[liste].produits.push(ean);
      }else{
        console.log('liste invalide');
      }
    }else{
      console.log('produit invalide');
    }
  }

  private savLists(){
    localStorage.setItem('listes_produits', JSON.stringify(this.listes));
  }

  public getCustomLists(){
    return this.listes.custom;
  }

  public getCustomList(id){
    return this.listes.custom[id];
  }

  public changeListName(id, name){
    this.listes.custom[id].nom = name;
    this.savLists();
  }

  public getListCourss(){
    return this.listes.courses;
  }
  public getListfav(){
    return this.listes.favoris;
  }
}
