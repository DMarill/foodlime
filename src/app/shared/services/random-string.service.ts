import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RandomStringService {

  possible = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

  constructor() { }

  public getRandomStr(long = 16){
    let text = "";
    for (let i = 0; i < long; i++) {
      text += this.possible.charAt(Math.floor(Math.random() * this.possible.length));
    }
    return text;
  }
}
