import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  private visible = false;
  private msg: string;
  private color: string;


  constructor() { }

  setAlert(msg: string, color: string = 'success'): void{
    this.msg = msg;
    this.color = color;
    this.visible = true;
  }

  reset(): void{
    this.visible = false;
    this.msg = null;
    this.color = null;
  }

  getVisibility(): boolean{
    return this.visible;
  }

  getMessage(): string{
    return this.msg;
  }

  getColor(): string{
    return this.color;
  }
}
