import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  private dbProduits: {};

  constructor(
      private http: HttpClient,
  ) {
      this.dbProduits = JSON.parse(localStorage.getItem('cache_produits'));
      if (this.dbProduits === null){
          this.dbProduits = {};
      }
      this.getProduct('gujyghih');
  }

  private getProductOFF(ean: string){
      // 3502110005922
      return this.http.get(environment.offURL + 'product/' + ean + '.json').toPromise()
          .then((data: any) => {
              if (data.status === 0){
                  return null;
              }
              let item = {
                nom: data.product.product_name_fr,
                generic_nom: data.product.generic_name_fr,
                brands: data.product.brands,
                categories: data.product.categories,
                quantity: data.product.quantity,
                img: data.product.image_url,
                img_thumb: data.product.image_thumb_url,
              };
              return item;
          }, error => {
            console.log(error);
          });
  }

  private savProduct(ean: string, data: any){
      this.dbProduits[ean] = data;
      localStorage.setItem('cache_produits', JSON.stringify(this.dbProduits));
  }

  public async getProduct(ean) {
      if(this.dbProduits[ean] !== undefined){
          console.log('en memoire');
      }else{
          const item = await this.getProductOFF(ean);
          if (item === null){
              return null;
          }
          this.savProduct(ean, item);
          console.log('save');
      }
      return this.dbProduits[ean];
  }
}

class produit{
    nom: string;
    generic_nom: string;
    brands: string;
    categories: string;
    quantity: string;
    img: string;
    img_thumb: string;
}
