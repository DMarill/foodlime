export const environment = {
  production: true,
  offURL: 'https://world.openfoodfacts.org/api/v0/',
};
